$( document ).ready(function() {

	$('.hamburger').click(function() {
		$(this).toggleClass('is-active');
		$('.top__menu').slideToggle();
	});

	var windowWidth = $(window).width();

	if(windowWidth < 768) {
		$('.top__menu__item__link.with__submenu').click(function(e){
			event.preventDefault(e);
			$(this).next('.top__submenu').slideToggle();
		});
	}
 
 /*Horizontal slider for card*/
	$('.card__slider__horizontal').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: true,
	  lazyLoad: 'ondemand',
	  asNavFor: '.card__slider__horizontal__nav'
	});

	$('.card__slider__horizontal__nav').slick({
	  slidesToScroll: 1,
	  asNavFor: '.card__slider__horizontal',
	  dots: false,
	  arrows: true,
	  focusOnSelect: true,
	  mobileFirst: true,
	  responsive: [
	  {
      breakpoint: 1000,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        dots: false,
        centerMode: false
      }
    },
    {
      breakpoint: 319,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    }
  ]
	});
	/*Horizontal slider for card END*/

	/*Vertical slider for card*/
	$('.vertical__card__slider').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  mobileFirst: true,
	  arrows: true,
	  responsive: [
	  {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        dots: false,
        vertical: true
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    },
    {
      breakpoint: 319,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    }
  ]
	});
	/*Vertical slider for card END*/

	/*Tabs for card*/
	$(".card__tabs").lightTabs();
	/*Tabs for card END*/

  /*Related products slider*/
  $('.related__products__slider').slick({
    slidesToScroll: 1,
    mobileFirst: true,
    arrows: true,
    responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    },
    {
      breakpoint: 319,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    }
  ]
  });
  /*Related products slider END*/

  /*Scroll to top btn*/
    $(window).scroll(function() {
        if ($(this).scrollTop() > 400) {
          $('.to__top, .request__call__btn').removeClass('slide-out-blurred-bottom');
          $('.to__top, .request__call__btn').addClass('slide-in-blurred-bottom');
        } else {
          $('.to__top, .request__call__btn').removeClass('slide-in-blurred-bottom');
          $('.to__top, .request__call__btn').addClass('slide-out-blurred-bottom');
        }
    });

    $('.to__top').click(function(toTop) {
        event.preventDefault(toTop);

        $('body,html').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });
  /*Scroll to top btn END*/

  /*Callback pop up*/
  $('.top__phones__callback, .request__call__btn').click(function(i) {
    event.preventDefault(i);
    $('.overlay').fadeIn();
    $('.--callback-popup').fadeIn();
  });

  $('.overlay, .pop__up__close').click(function(){
    $('.overlay').fadeOut();
    $('.pop__up').fadeOut();
  });
  /*Callback pop up END*/


  $('.main__service__section__slider').slick({
    slidesToScroll: 1,
    mobileFirst: true,
    arrows: true,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    },
    {
      breakpoint: 319,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    }
  ]
  });

  /*Popular items on main*/
  $('.popular__items__slider').slick({
      slidesToScroll: 1,
      mobileFirst: true,
      infinite: true,
      arrows: true,
      responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 999,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 319,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false
        }
      }
    ]
  });

  /*Projects slider on main*/
  $('.main__completed__projects__slider').slick({
    slidesToScroll: 1,
    mobileFirst: true,
    infinite: false,
    arrows: true,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 319,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false
      }
    }
  ]
  });

  /*Similar articles slider*/
  $('.similar__articles__slider').slick({
    slidesToScroll: 1,
    mobileFirst: true,
    infinite: false,
    arrows: true,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 319,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false
      }
    }
  ]
  });

});